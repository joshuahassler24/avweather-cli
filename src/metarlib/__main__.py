#!/usr/bin/python
import argparse
import json
import requests
import sys
import xml.etree.ElementTree as ET
import pprint

#def notify(title, text):
#    os.system("""
#              osascript -e 'display notification "{}" with title "{}"'
#              """.format(text, title))

#    title = "New METAR for %s" % metar.find('station_id').text
#    notify(title, metar.find('raw_text').text)

class METAR( object ):
    def __init__( self, metarXML ):
        for element in metarXML:
            setattr( self, element.tag, element.text )
        self.metarAttrs = [ e.tag for e in metarXML ]

    def __str__( self ):
        return "{metar}".format( metar=self.raw_text )

    def getAttrs( self ):
        return { e: getattr( self, e ) for e in self.metarAttrs }

def fetchMetars( codes ):
    baseURL = "https://aviationweather.gov/adds/dataserver_current/httpparam?datasource=metars&requestType=retrieve&format=xml&mostRecentForEachStation=constraint&hoursBeforeNow=1.25&stationString={stations}"
    codeStr = ",".join( codes )
    response = requests.get( baseURL.format( stations=codeStr ) )
    if not response.ok:
        return None

    root = ET.fromstring( response.content )
    metars = { metar.find( 'station_id' ).text : metar for metar in root.find( 'data' ).findall( 'METAR' ) }
    return [ METAR( metars[ code ] ) for code in codes if code in metars]

def main():
    parser = argparse.ArgumentParser( description='Fetch METARs' )
    parser.add_argument( 'codes', metavar='C', type=str, nargs='+',
                         help='ICAO code of the airport to fetch a METAR for' )
    parser.add_argument( '--json', '-j', action='store_true', help='output as machine readable JSON' )
    parser.add_argument( '--oneline', '-l', action='store_true', help='output as one line' )
    args = parser.parse_args()

    metars = fetchMetars( args.codes )
    if not metars:
        sys.stderr.write("Error fetching METARs")
        return -1

    for metar in metars:
        if args.json:
            fmtArgs = {}
            if not args.oneline:
                fmtArgs[ "indent" ] = 4
            print( json.dumps( metar.getAttrs(), **fmtArgs ) )
        else:
            out = "{title}: {body}" if args.oneline else "{title}\n{body}"
            out = out.format( title="METAR for {station}".format( station=metar.station_id ),
                              body="{metar}".format( metar=metar ) )
            print( out )

if __name__ == "__main__":
    sys.exit( main() )
