import setuptools

with open( "README.md", "r", encoding="utf-8" ) as fp:
    ld = fp.read()

with open( "requirements.txt", "r", encoding="utf-8" ) as fp:
    req = fp.readlines()

setuptools.setup(
    name="metar-cli",
    version="0.1.0",
    author="Joshua Hassler",
    author_email="joshua@hassler.dev",
    description="CLI tool for querying US metars",
    long_description=ld,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/joshuahassler24/avweather-cli",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
        "Operating System :: OS Independent",
    ],
    package_dir={"": 'src'},
    packages=setuptools.find_packages( where="src" ),
    install_requires=req,
    python_requires='>3.6',
    entry_points={
        'console_scripts': [
            'metar = metarlib.__main__:main',
        ],
    }
)
