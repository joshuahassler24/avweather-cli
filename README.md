[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0) [![pipeline status](https://gitlab.com/joshuahassler24/avweather-cli/badges/main/pipeline.svg)](https://gitlab.com/joshuahassler24/avweather-cli/-/commits/main)

# METAR CLI
This package provides an interface for querying US metars in the command line. Some
classes and methods can also be import as a library for people looking to integrate
this with their own packages.

## CLI Options
* `--json, -j`: change the output to machine readable JSON
* `--oneline, -l`: format the output as one line

## Example Usage
```
> meatar --oneline KASH
METAR for KASH: KASH 041256Z 26006KT 10SM FEW022 23/19 A2988 RMK AO2 SLP121 T02280194
> metar -j -l KBOS
{"raw_text": "KBOS 041254Z 23012KT 10SM FEW013 SCT090 BKN130 BKN220 23/18 A2989 RMK AO2 SLP120 T02330178",
"station_id": "KBOS", "observation_time": "2021-06-04T12:54:00Z", "latitude": "42.37", "longitude": "-71.02",
"temp_c": "23.3", "dewpoint_c": "17.8", "wind_dir_degrees": "230", "wind_speed_kt": "12",
"visibility_statute_mi": "10.0", "altim_in_hg": "29.890747", "sea_level_pressure_mb": "1012.0",
"quality_control_flags": "\n        ", "sky_condition": null, "flight_category": "VFR", "metar_type": "SPECI",
"elevation_m": "4.0"}
```

## Installation
### pip
```(bash)
curl -L "https://gitlab.com/joshuahassler24/avweather-cli/-/jobs/artifacts/main/raw/dist/metar_cli-0.1.0-py3-none-any.whl?job=build-artifact" --output metar_cli-0.1.0-py3-none-any.whl
pip install metar_cli-0.1.0-py3-none-any.whl
rm metar_cli-0.1.0-py3-none-any.whl
```

### build from source
```(python)
pip install build
python -m build
pip install dist/metar_cli-0.1.0-py3-none-any.whl
```

## License
This project is licensed under AGPLv3 or later. For more inforation on AGPL see [here](https://www.gnu.org/licenses/agpl-3.0.en.html)
